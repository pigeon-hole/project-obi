#This is setup to be used with Python 2.7

#Makes in compatible with Python 3 print()
from __future__ import print_function

#Sys used to find of the size of message when using radio.write()
import sys

import time                                                                          
from RF24 import *

radio = RF24(22,0)

#Determines if the RPi will be recieving or sending calls
#radio_number(1) =  sending, radio_number(0) = revieving
radio_number = '1' 

#Sets up pipes that are used for communication
#address = ["Node1", "Node2"]
address = [0xF0F0F0F0E1, 0xF0F0F0F0D2]

#stuct payload
close_window = 1
open_window = 0
#1 to close the window, 0 to open it
message = '01'

#Boolean value that keeps track if the window is closed
#or open. If False the window is open. We assume the window
#starts out open, to begin tacking weather.
is_closed = False

millis = lambda: int(round(time.time() * 1000))

#Setting up radio
radio.begin()
radio.enableDynamicPayloads()
#radio.setAutoAck(1)
radio.setRetries(5,15)

if radio_number == '0':
    radio.openWritingPipe(address[1])
    radio.openReadingPipe(1,address[0])
elif radio_number == '1':
    radio.openWritingPipe(address[0])
    radio.openReadingPipe(1, address[1])
else :
    print("Radio number wasn't chosen properly")
    exit()

radio.startListening()
radio.printDetails()

#Will loop forever
while 1:

    #Stop radio to be able to send messages
    radio.stopListening()
    
    #send the message
    print("Sending radio message : {}".format(message[close_window]))
    radio.write(message[close_window])
    
    #Start listening again to get a response back
    radio.startListening()

    # Wait here until we get a response, or timeout
    started_waiting_at = millis()
    timeout = False
    while (not radio.available()) and (not timeout):
        if (millis() - started_waiting_at) > 500:
            timeout = True

     # Describe the results
    if timeout:
        print('failed, response timed out.')
    else:
        # Grab the response, compare, and send to debugging spew
        len = radio.getDynamicPayloadSize()
        is_closed = radio.read(len)
        print("Recieved response {}".format(is_closed))

    print("Sent response")
    radio.startListening()
