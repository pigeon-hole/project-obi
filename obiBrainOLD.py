#This works with Python 2.7

#Makes in compatible with Python 3 print()
from __future__ import print_function

#Sys used to find of the size of message when using radio.write()
import sys

import time                                                                          
from RF24 import *

#Imports for weather detection
import urllib2
import json
#import time

#Find zip code usin website based on ip address
f = urllib2.urlopen('http://freegeoip.net/json/')
json_string = f.read()
f.close()
location = json.loads(json_string)
#print(location)
#Assign zip code
location_city = location['city']
location_state = location['region_name']
location_country = location['country_name']
location_zip = location['zip_code']

#Use webste to automatically get ip address and weather
f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+location_zip+'.json')
json_string = f.read()
parsed_json = json.loads(json_string)
location = parsed_json['location']['city']
temp_f = parsed_json['current_observation']['temp_f']

#Radio settings
radio = RF24(22,0)

#Determines if the RPi will be recieving or sending calls
#radio_number(1) =  sending, radio_number(0) = revieving
radio_number = '1' 

#Sets up pipes that are used for communication
#address = ["Node1", "Node2"]
address = [0xF0F0F0F0E1, 0xF0F0F0F0D2]

#stuct payload
close_window = 1
open_window = 0
#1 to close the window, 0 to open it
message = '01'

#Boolean value that keeps track if the window is closed
#or open. If False the window is open. We assume the window
#starts out open, to begin tacking weather.
window_closed = 0


millis = lambda: int(round(time.time() * 1000))

#Setting up radio
radio.begin()
radio.enableDynamicPayloads()
#radio.setAutoAck(1)
radio.setRetries(5,15)

if radio_number == '0':
    radio.openWritingPipe(address[1])
    radio.openReadingPipe(1,address[0])
elif radio_number == '1':
    radio.openWritingPipe(address[0])
    radio.openReadingPipe(1, address[1])
else :
    print("Radio number wasn't chosen properly")
    exit()

radio.startListening()
radio.printDetails()

while(1):
	
	radio.startListening();
    
	#Check to see if recieving message from Obi Body   
  	if(radio.available()):
  		
		#Whlie getting message about window
		while (radio.available()):
			#print("Hello world")
			len = radio.getDynamicPayloadSize()
			#print("Hello world1")
			if len:
				#global window_closed
				#print("Hello world2")
				#print("Radio read {}".format(radio.read(len)))
				recieved_message = radio.read(len)
				print("Value in message {}".format(recieved_message))
				if( recieved_message == "op" ):
					window_closed = 0
					print("New value in window_closed is={}".format(window_closed))
				elif(recieved_message == "cl"):
					window_closed = 1
					print("New value in window_closed is={}".format(window_closed))
	
				time.sleep(5)
				#time.sleep(15)
				#exit()
				#print(len)
				#continue
    		#Continue if the length was incorrect
    		else:
    			continue
    			
			#print(radio.read(len))
    		#window_closed = radio.read(len)
    		#radio.read(window_closed, len)
    		#print("Hello world")
       		#Print message recieved from Obi Body
        	#print("Message from Obi Body={}".format(window_closed))
		
        	     
	
	print()
    #If the window is open we start checking the weather
	#time.sleep(5)
	if(window_closed == 0):
		
		print("In window open")
		precipitation = int((parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop']))
		weather = (parsed_json['forecast']['txt_forecast']['forecastday'][0]['title'])

		weather = "Rain";

		if(weather == "Rain" or weather == "Sleet" or weather == "Snow" or weather == "Flurries" or weather == "Thunderstorm"):
		    #print("We are closing the window!")
		        #send the message

		    radio.stopListening();

		    print("Sending radio message : {}".format(message[close_window]))
		    radio.write(message[close_window])

		    #Start listening again to get a response back
		    radio.startListening()

			#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		    # Wait here until we get a response, or timeout
		    #started_waiting_at = millis()
		    #timeout = False
		    #while (not radio.available()) and (not timeout):
		 		#Timeout if the window doesn't close in a minute
		        #if(millis() - started_waiting_at) > 60000:
		            #timeout = True

			 # Describe the results
			#if timeout:
			  #  print('failed, response timed out.')
			#else:
				#print('in else statment')
				# Grab the response, compare, and send to debugging spew
				#len = radio.getDynamicPayloadSize()
				#recieved_message = radio.read(len)
				
				#if (recieved_message == "op") :
					#window_closed = 0
				#elif(recieved_message == "cl"):
					#window_closed = 1
					
				#print("Recieved response {}".format(window_closed))
				#window_closed = 0;
		else:
			print("We are checking again to see if the weather changes!")        
			if(precipitation > 90):
		   		#time.sleep(300)
		  		time.sleep(1)
			elif(precipitation <= 90 and precipitation > 75):
				#time.sleep(900)
				time.sleep(5)
			elif(precipitation <= 75 and precipitation > 50):
				#time.sleep(1800)
				time.sleep(10)
			elif(precipitation <= 50 and precipitation >= 0):
				#time.sleep(3600)
				time.sleep(15)
				
	#If the windows is closed
	elif window_closed:
		print("The window is currently closed"	)
	else:
		print("Window position unknown")
#TODO: what is this
#f.close()




