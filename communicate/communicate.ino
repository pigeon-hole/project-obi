#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"

//int msg[1];
//const uint64_t pipe = 0xE8E8F0F0E1LL;

//Usage : RF24 (uint8_t _cepin, uint8_t _cspin)
RF24 radio(7,8);


//Addresses to be used when reading and writing
const uint8_t address[][6] = {"1Node", "2Node"};
//address[0] = reading address
//address[1] = writing address

/*
//Send signal
void setup(void){
 Serial.begin(9600);
 radio.begin();
 radio.openWritingPipe(pipe);
}

void loop(void){
 if (Serial.available() > 0) {
  msg[0] = 111;
  radio.write(msg, 1);
  Serial.println("Message sent.");
  int data = Serial.read();
  Serial.println(data, DEC);
 }
}
*/

//Recieve signal
void setup(){
  Serial.begin(57600);
  Serial.println("Nrf24L01 Receiver Starting");
  radio.setAutoAck(1);                    // Ensure autoACK is enabled
  radio.setRetries(15,15);                // Max delay between retries & number of retries
  Serial.println("Radio avaliable");
  radio.begin();
  
  //First pipe opened for reading with address pipie[0]
  radio.openReadingPipe(1,address[0]); 
  //radio.openReadingPipe(3,pipe);
  radio.startListening();
  Serial.println("Radio avaliable");
}

void loop(void){
 /*if (radio.available()){
   Serial.println("Radio is available");
   bool done = false;    
   while (!done){
     done = radio.read(msg, 1);      
     Serial.println(msg[0]);
     if (msg[0] == 111){
      Serial.println("Signal recieved.");
     }
     else {
      Serial.println("Signal not recived.");
     }
     delay(500);
   }
 }
 else{
  Serial.println("No radio available");
  delay(50);
 }*/
  if(radio.available()){
    Serial.println("Radio avaliable");
    bool done = false;
    //If action = 1 (close window);  
    short action;  
    while (radio.available()){
      Serial.println("Waiting for data");
      radio.read(&action, sizeof(short));
    }
    if(action == 1){
      Serial.println("Close the window");
    }
    else{
      Serial.println("Open the window");
    }
  }
  else{
    Serial.println("Radio not working properly");
  }
  
  delay(1000);
}

