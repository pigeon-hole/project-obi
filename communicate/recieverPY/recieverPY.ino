#include <SPI.h>
#include <string.h> //strcpy
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/*Determines if the Arduino is revieving signals
 * or sending them. 1 = listen; 0 = send
 */
bool radioNumber = 1;

//Pins that the CE and CSN Pin are plugged into
int cePin = 7;
int csnPin = 8;

RF24 radio(cePin ,csnPin);
//
//const int role_pin = 5;

const uint64_t addresses[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
//byte addresses[][6] = {"1Node","2Node"};

//Currently the message can either be a 0 or 1 
const int maxMessagelen = 2;

//mexMessagelen + terminating character
char message[maxMessagelen + 1];

//Actions that are checked when message recieved
const char * openWindow = "0";
const char * closeWindow = "1";

/*Keeps track of the window being closed. By default
 * the window is assumed open. This isn't a boolean, because
 * they are harder to send using the transcievers. 
 */
char * isClosed = "0";

void setup() {

//  // set up the role pin
//  pinMode(role_pin, INPUT);
//  digitalWrite(role_pin,HIGH);
//  delay(20); // Just to get a solid reading on the role pin

  Serial.begin(9600);
 
   printf_begin();
   radio.begin();   
   
   //Enabled to get len of the payload
   //radio.enableDynamicPayloads();
   
   //radio.setAutoAck(1); // Ensure autoACK is enabled
 
   radio.enableDynamicPayloads();
   radio.setRetries(5,15);
     
  // Open a writing and reading pipe on each radio, with opposite addresses
  if(radioNumber){
    //Pipes set to listen
    radio.openWritingPipe(addresses[1]);
    radio.openReadingPipe(1,addresses[0]);
  }
  else{
    //Pipes set to sender
    radio.openWritingPipe(addresses[0]);
    radio.openReadingPipe(1,addresses[1]);
  }
   
   radio.startListening();
   radio.printDetails();   
   Serial.println("Radio has started listening");
}

void loop()
{
    if(radio.available())
    {	    
        //Serial.println("In if statement");
        uint8_t len;
        //Get the message
        while(radio.available()){
          	len = radio.getDynamicPayloadSize();
		
    			//If something is wrong with len, ignore the payload
    			if(!len)
    				continue;
    			else
          		radio.read(message, len);
        }

      //char temp = message;
   		printf("The message recieved was %s\n", message);    

      //Check to see what type of message was sent
      if(strcmp(closeWindow, message)==0)
      {
        printf("The message was to close the window\n");  
        //TODO: Close window function

        //Send the result back
        radio.stopListening();
        
        //If window closed properly
          isClosed = "1";
        radio.write(isClosed, sizeof(isClosed));
        radio.startListening();
      }
      else if(strcmp(closeWindow, message)==0)
      {
        printf("The message was to open the window\n");
        //TODO: Open window function

        //Send the result back
        radio.stopListening();
        
        //If window opened properly
          isClosed = "0";
        radio.write(isClosed, sizeof(isClosed));
        radio.startListening();
      }
      else{
        printf("!!!!!Invalid message was sent!!!!\n");

        //Send result back
      }   
    }
  delay(100);
}

