#include <SPI.h>
#include <string.h> //strcpy
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/*Determines if the Arduino is revieving signals
 * or sending them. 0 = listen; 1 = send
 */
bool radioNumber = 0;

//Pins that the CE and CSN Pin are plugged into
int cePin = 7;
int csnPin = 8;

RF24 radio(cePin ,csnPin);

byte addresses[][6] = {"1Node","2Node"};

//struct payload_request_t
//{
//  uint8_t number;
//  uint8_t destination;
//  char message[14];
//};
//
//struct payload_general_t
//{
//  uint8_t number;
//  char message[15];
//};


//Message to be sent 
struct payload{
  uint8_t action;
  char result[8];
}message;


unsigned long got_time; 

void setup() {

  Serial.begin(9600);
 
   printf_begin();
   radio.begin();   
   radio.setAutoAck(1); // Ensure autoACK is enabled
   radio.setRetries(15,15);
   //radio.enableDynamicPayloads();

  // Open a writing and reading pipe on each radio, with opposite addresses
  if(radioNumber){
    //Pipes set to send
    radio.openWritingPipe(addresses[1]);
    radio.openReadingPipe(1,addresses[0]);
  }
  else{
    //Pipes set to listen
    radio.openWritingPipe(addresses[0]);
    radio.openReadingPipe(1,addresses[1]);
  }
   
   radio.startListening();
   radio.printDetails();   
   Serial.println("Radio has started listening");
}

void loop()
{

    if(radio.available())
    {
        Serial.println("In if statement");
        while(radio.available()){
          radio.read(&message, sizeof(message));
        }
        radio.stopListening();                                        // First, stop listening so we can talk
        strcpy(message.result, "success");   
        radio.write( &message, sizeof(message) );              // Send the final one back.      
        radio.startListening();                                       // Now, resume listening so we catch the next packets.     
        Serial.print(F("Sent response "));
        Serial.println(message.action);
    }
  delay(100);
}

