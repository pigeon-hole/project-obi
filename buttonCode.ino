const int toggleSwitch = 2;
const int button1 = A1;
const int button2 = A2;
const int motorTerminal1 = 3;
const int motorTerminal2 = 4;
const int enablePin = 9;

void setup() {
	pinMode( toggleSwitch, INPUT);
	pinMode( motorTerminal1, OUTPUT);
	pinMode( motorTerminal2, OUTPUT);
	
	digitalMode( enablePin, HIGH);
}

void loop() {
	
	if( analogRead( button1 ) != 0 ){
		if( digitalRead(toggleSwitch == HIGH ){
			digitalWrite(motorTerminal, LOW);
			digitalWrite(motorTerminal, HIGH);
		}
		else{
			digitalWrite(motorTerminal1, HIGH);
			digitalWrite(motorTerminal2, LOW);
		}
	}

	if( analogRead( button2 ) != 0 ){
		if( digitalRead(toggleSwitch == HIGH ){
			digitalWrite(motorTerminal, HIGH);
			digitalWrite(motorTerminal, LOW);
		}
		else{
			digitalWrite(motorTerminal1, LOW);
			digitalWrite(motorTerminal2, HIGH);
		}
	}
}