/* This program is able to open and close the window
 *
 *
 *  Using signals received from either the buttons or the raspberry pi, we make the motor spin depending on the toggle switch
 *  @Authors: Bryan Cantos
 *            Daniel Liu
 *            Adrian Khaskin
 */



#include <Servo.h>
#include <SPI.h>
#include <string.h> //strcpy
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

/***********HARDWARE CONFIGURATION**************/

//Button pins
int openButton = A1;
int closeButton = A2;

// Determines which way motor spins
  
int toggleSwitch = 2;
//IF 0 spins clockwise
//IF 1 spins count-clockwise
int toggleValue;

Servo myServo;

//Stall Pin (Determines if Motor is stalling)
int stallPin = A3; 
int stallThreshold = 1010;

/************RADIO CONFIGURATION**************/

/*Determines if the Arduino is revieving signals
 * or sending them. 1 = listen; 0 = send
 */
bool radioNumber = 1;
bool buttonOpen = 1;

//Pins that the CE and CSN Pin are plugged into
int cePin = 7;
int csnPin = 8;

RF24 radio(cePin ,csnPin);

const uint64_t addresses[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };

//Currently the message can either be a 0 or 1 
const int maxMessagelen = 2;

//mexMessagelen + terminating character
char message[maxMessagelen + 1];

//Actions that are checked when message recieved
const char * openMessage = "0";
const char * closeMessage = "1";

/*Keeps track of the window being closed. By default
 * the window is assumed open. This isn't a boolean, because
 * they are harder to send using the transcievers. 
 */
char * isClosed = "0";
bool sendOpen = false;
bool sendClosed = true;
unsigned long startTime;

void setup() {
  /***********HARDWARE SETUP**************/
  
  //Motor pins
  pinMode(toggleSwitch, INPUT);

  //Button pins
  pinMode(openButton, INPUT);
  pinMode(closeButton, INPUT);
  pinMode(stallPin, INPUT);
  myServo.attach(A0);
  myServo.write(91);

  Serial.begin(9600);
  printf_begin();

  /***********RADIO SETUP**************/
  radio.begin();   
  radio.enableDynamicPayloads();
  radio.setRetries(5,15);
  // Open a writing and reading pipe on each radio, with opposite addresses
  if(radioNumber){
    //Pipes set to listen
    radio.openWritingPipe(addresses[1]);
    radio.openReadingPipe(1,addresses[0]);
  }
  else{
    //Pipes set to sender
    radio.openWritingPipe(addresses[0]);
    radio.openReadingPipe(1,addresses[1]);
  }
  //Radio starts waiting for signal
  radio.startListening();
  radio.printDetails();   
  Serial.println("Radio has started listening");
}

void loop() {
    
    toggleValue = digitalRead(toggleSwitch);
  
    //TODO: Switch back to openButton
    while(digitalRead(openButton) == LOW /*&& analogRead(stallPin) > stallThreshold*/){
      openWindow();
      //Stop listening to send signal
      sendOpen = true;
      startTime = millis();
      //If window closed properly
      //The window isn't closed anymore\
    }
    if(sendOpen && (millis() - startTime > 100)){
      myServo.write(91);
      printf("Sending open message to Obi Brain.\n");
      radio.stopListening();
      isClosed = "op";
      radio.write(isClosed, sizeof(isClosed));
      //Message was sent, don't need to send it anymore
      sendOpen = false;
      radio.startListening();
    }
    while(digitalRead(closeButton) == LOW && analogRead(stallPin) > stallThreshold){
      closeWindow();
      Serial.println(analogRead(stallPin));
    }
    myServo.write(91);
//
    if(radio.available())
      getMessage();
      
  delay(15);
}

//Opens the window
void openWindow(){
    if(toggleValue == 0)
      myServo.write(180);
    else
      myServo.write(0);
}

//Closes the window
void closeWindow(){
    if(toggleValue == 0)
      myServo.write(0);
    else
      myServo.write(180);
}

void getMessage(){

  //Stores the length of the message
  uint8_t len;
  //Get the message
  while(radio.available()){
      len = radio.getDynamicPayloadSize();

    //If something is wrong with len, ignore the payload
    if(!len)
      continue;
    else
        radio.read(message, len);
  }

  //char temp = message;
  printf("The message recieved was %s\n", message);    

  //If the message was to close the window
  if(strcmp(closeMessage, message)==0)
  {
    printf("The message was to close the window\n");  
    //TODO: Close window function

    //Send the result back
     closeWindow();
     radio.stopListening();
    Serial.println("The window is closed.");  
    //If window closed properly
    isClosed = "cl";
    
    radio.write(isClosed, sizeof(isClosed));
    radio.startListening();
     delay(6000);

    myServo.write(91);
    
  }
  //If the message was to open the window
  else if(strcmp(openMessage, message)==0)
  { 
    printf("The message was to open the window\n");
    //TODO: Open window function

    //Send the result back
    radio.stopListening();
    
    //If window opened properly
    isClosed = "op";
    radio.write(isClosed, sizeof(isClosed));
    radio.startListening();
  }
}
