"""
By Daniel Liu
Obibrain combining weather checking and website. Adding in message created some error so it we seperated. Used the flask folder from Adafruit's github.
Replaced Adafruits flask_listener.py with this code and just ran it. To run it replace the flask_listener.py and run it and to get to the website
type in https://{ip-address}:5000. The ip-address is the where your computer or raspberry pi is running this code.
"""
#flask libraries
from flask import Flask
from flask import request
from flask import render_template
from flask import redirect, url_for

from raspipe import RasPipe
#weather checking libraries and threading
import urllib2
import json
import time
import threading

f = urllib2.urlopen('http://freegeoip.net/json/')
json_string = f.read()
f.close()
location = json.loads(json_string)

#current zip code
global zipc 
zipc = location['zip_code']
#status of window
global window_closed 
window_closed = 1
#if user wants to close window
global user_close 
user_close = 0
#access wunderground
global f 
f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+zipc+'.json')
global json_string
json_string = f.read()
global parsed_json
parsed_json = json.loads(json_string)
#chance of rain
global pop
precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
#weather forecast
global forecast
weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']

app = Flask(__name__)

rp = RasPipe(None)
rp.input_lines.append('starting up...')
rp.render_frame()

#start webpage
@app.route('/')
def index():
    return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)

#get zip code
@app.route('/send', methods=['POST'])
def send():
    #if user sent zip code
    if len(request.form['address']) == 5 and request.form['address'].isdigit():
        #get zip code from website
        global zipc
        zipc = int(request.form['address'])
        #change address based on zip code
        global f
        f = urllib2.urlopen('http://api.wunderground.com/api/bb9c99472b38a6a1/forecast/geolookup/conditions/q/'+ request.form['address'] +'.json')
        global json_string
        json_string = f.read()
        global parsed_json
        parsed_json = json.loads(json_string)
        #assign percipitation to vaariable
        global precipitation
        precipitation = int(parsed_json['forecast']['txt_forecast']['forecastday'][0]['pop'])
        #assign weather to variable
        global weather
        weather = parsed_json['forecast']['txt_forecast']['forecastday'][0]['icon']
        rp.input_lines.append(request.form['address'])
        rp.render_frame()
        #update webpage
        return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))

#get button pressing
@app.route('/window', methods=['POST'])
def window():
    #if user clicked button
    if request.method == 'POST':
        #if user clicked open
        if request.form['submit'] == 'Open':
            global user_close
            user_close = 0
            global window_closed
            window_closed = 0
            rp.input_lines.append('Open')
            rp.render_frame()
            #update webpage
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
        #if user clicked open
        elif request.form['submit'] == 'Close':
            global user_close
            user_close = 1
            global window_closed
            window_closed = 1
            rp.input_lines.append('Close')
            rp.render_frame()
            #update webpage
            return render_template('index.html', rp=rp, zip_code = zipc, window = window_closed)
    return redirect(url_for('index'))

#function that checks weather
def check_weather():
    #keep checking
    while(1):
        global window_closed
        #if window is not closed
        if(window_closed == 0):
            global zipc
            print zipc
            global weather
            print weather
            global precipitation
            print precipitation
            #if bad weather close window
            if(weather == "rain" or weather == "sleet" or weather == "snow" or weather == "flurries" or weather == "thunderstorm"):
                print("We are closing the window!")
                global window_closed
                window_closed = 1;
            else:
                print("We are checking again to see if the weather changes!")        
            #check weather based on percipitation level in intervals
            if(precipitation > 90):
                #time.sleep(300)
                print ("precipitation > 90")
                time.sleep(1)
            elif(precipitation <= 90 and precipitation > 75):
                #time.sleep(900)
                print ("precipitation <= 90 and precipitation > 75")
                time.sleep(5)
            elif(precipitation <= 75 and precipitation > 50):
                #time.sleep(1800)
                print ("precipitation <= 75 and precipitation > 50")
                time.sleep(10)
            elif(precipitation <= 50 and precipitation >= 0):
                #time.sleep(3600)
                print ("precipitation <= 50 and precipitation >= 0")
                time.sleep(15)

#main
if __name__ == '__main__':
    #thread to check weather
    check_thread = threading.Thread(target = check_weather)
    #start thread
    check_thread.start()
    #host website using flask
    app.run(host='0.0.0.0')
